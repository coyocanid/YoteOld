#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"

#include "record_store.h"

MODULE = Data::RecordStore::XS		PACKAGE = Data::RecordStore::XS

RecordStore *
store_open(directory)
     char *directory
CODE:
    RETVAL = open_store( directory );
OUTPUT:
    RETVAL

void    
store_openy(directory)
    char *directory
PPCODE:
     RecordStore * store = open_store( directory );
     EXTEND( SP, 3 );
     PUTBACK;
     ST(0) = sv_2mortal(newSVsv(store));
     ST(1) = PTR2UV(store);
     ST(2) = store;
     SPAGAIN;
     XSRETURN( 3 );
    

    
char *
store_fetch( store, rid )
     RecordStore * store
     uint64_t rid
CODE:
     RETVAL = fetch( store, rid );
OUTPUT:
    RETVAL
        
uint64_t
store_stow( store, data, rid, write_amount )
     RecordStore * store
     char * data
     uint64_t rid
     uint64_t write_amount
CODE:
     RETVAL = stow( store, data, rid, write_amount );
OUTPUT:
     RETVAL

    
uint64_t
store_next_id( store )
     RecordStore * store
CODE:
     RETVAL = next_id( store );
OUTPUT:
     RETVAL
       
void
store_delete( store, rid )
     RecordStore * store
     uint64_t rid
CODE:
    delete_record( store, rid );

void
store_empty_recycler( store )
     RecordStore * store
CODE:
    empty_recycler( store );

void
store_unlink( store )
     RecordStore * store
CODE:
    unlink_store( store );

void
store_cleanup( store )
     RecordStore * store
CODE:
    cleanup_store( store );
        
    
void
store_recycle( store, rid )
     RecordStore * store
     uint64_t rid
CODE:
    recycle_id( store, rid );

int
store_has_id( store, rid )
     RecordStore * store
     uint64_t rid
CODE:
    RETVAL = has_id( store, rid );
OUTPUT:
    RETVAL
    
uint64_t
entry_count_store( store )
    RecordStore * store
CODE:
    RETVAL = store_entry_count( store );
OUTPUT:
    RETVAL

Silo *
store_get_silo( store, sidx )
    RecordStore * store
    unsigned int sidx
CODE:
    PREP_SILO;
    SET_SILO( store, sidx );
    RETVAL = SILO;
OUTPUT:
    RETVAL

void
silo_cleanup( silo )
     Silo * silo
CODE:
    cleanup_silo( silo );
        

    
void
store_empty( store )
     RecordStore * store
CODE:
    empty_store( store );

Transaction *
store_create_transaction( store )
     RecordStore * store
CODE:
    RETVAL = open_transaction( store, 1 );
OUTPUT:
    RETVAL

void
store_list_transactions( store )
     RecordStore * store
PPCODE:
     Transaction * trans;
     int i = 0;
     int j;
     Transaction ** transes = list_transactions( store );
     while ( NULL != (trans = transes[ i++ ] ) );
     EXTEND( SP, i );
     for ( j=0; j<i; j++ )
       {
         //         PUSHs( sv_2mortal( newSVpv( transes[j], 0 ) ) );
       }
     SPAGAIN;
     XSRETURN( i );

MODULE = Data::RecordStore::XS		PACKAGE = Data::RecordStore::Silo::XS

Silo *
silo_open( directory, size )
    char * directory
    uint64_t size
CODE:
    RETVAL = open_silo( directory, size );
OUTPUT:
    RETVAL    

uint64_t
next_id_silo( silo )
     Silo * silo
CODE:
     RETVAL = silo_next_id( silo );
OUTPUT:
     RETVAL

uint64_t
entry_count_silo( silo )
     Silo * silo
CODE:
     RETVAL = silo_entry_count( silo );
OUTPUT:
     RETVAL

int
put_record_silo( silo, sid, data, write_size )
     Silo * silo
     uint64_t sid
     char * data
     uint64_t write_size
CODE:
     RETVAL = silo_put_record( silo, sid, data, write_size );
OUTPUT:
     RETVAL

void
get_record_silo( silo, templ, templ_size, sid )
     Silo * silo
     char * templ
     unsigned int templ_size
     uint64_t sid    
PPCODE:
     PUTBACK;
     char * r = silo_get_record( silo, sid );
     int i = unpackstring( templ, templ+templ_size, r, r + silo->record_size, SVt_PVAV );
     EXTEND( SP, i );
     SPAGAIN;
     XSRETURN( i );
    
    
int
_silo_set_max_records( silo, recs )
    Silo * silo
    int recs
CODE:
    silo->file_max_records = recs;
    RETVAL = silo->file_max_records;
OUTPUT:
    RETVAL

void
silo_unlink( silo )
     Silo * silo
CODE:
    unlink_silo( silo );

    
MODULE = Data::RecordStore::XS		PACKAGE = Data::RecordStore::Transaction::XS


